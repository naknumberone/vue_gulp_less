'use strict'

var app = function() {
    var areaFilling;
    Vue.component('cvs', {
    props: { reqF: 0, reqA: 0, id: 0 },
    data: function() {
      return {

      }
    },
    template: '<div class="canvasDiv"><canvas id="canvas" style="background-color: yellow"></canvas></div>',
    mounted: function(){
      areaFilling = new window.AreaFilling('canvas');
      areaFilling.init(this.reqA, this.reqF);
      var input = this.id;
      setTimeout(function(){areaFilling.restoreRects(input); }, 500);
    }
  });

  const One = { template: '<cvs reqF="/src/mongo.php?set=1" reqA="/src/area.php?set=1" :id="$route.params.id"></cvs>' };
  const Two = { template: '<cvs reqF="/src/mongo.php?set=2" reqA="/src/area.php?set=2" :id="$route.params.id"></cvs>' };
  const Three = { template: '<cvs reqF="/src/mongo.php?set=3" reqA="/src/area.php?set=3" :id="$route.params.id"></cvs>' };

  const routes = [
    { path: '/1/:id', component: One },
    { path: '/2/:id', component: Two },
    { path: '/3/:id', component: Three },
  ];

  const router = new VueRouter({
    routes
  });

  const app = new Vue({
    router,
    data: {
      inputName: '',
      inputMail:'',
      inputPhone: '',
      inputMsg: '',
      screen: '',

      formSeen: true,
      formMessage: ''
    },
    methods: {
      send: function(){
        app.screenshot();

        var allInput = {
          name: this.inputName,
          mail: this.inputMail,
          phone: this.inputPhone,
          msg: this.inputMsg,
          screen: this.screen
         };

        var postJSON = JSON.stringify(allInput);
        axios.post('/src/processingForm.php', postJSON )
          .then(function (response) {
            console.log(response.data);
            app.formSeen = false;
            app.formMessage = 'Данные успешно отправлены!';
          })
          .catch(function (error) {
            app.formMessage = 'Ошибка отправки данных'+error.message;
            console.log("ERROR:"+error.message);
          });
      },
      screenshot: function(){
        var cvs = document.getElementById('canvas');
        app.screen = cvs.toDataURL();
        var data = app.screen.replace('data:image/png;base64,',"");
        console.log(app.screen);
      },
      add: function(){
        areaFilling.addRect();
      }
    }
  }).$mount('#app');

}();
