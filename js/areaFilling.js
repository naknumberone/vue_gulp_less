window.AreaFilling = function(id){
  var canvas = document.getElementById(id);
  var ctx = canvas.getContext("2d");
  var area = {
    width: 5, /*Размер одной стороны в секторах*/
    height: 5,
    step: 100, /*Размер сектора*/
    marginRight: 400 /*Отсуп справа от области, где будут находиться
    неустановленные фигуры*/
  };
  var offsetRectsY = 0; /*Хранит текущее смещение фигур по оси Y, получаемое при скроллинге*/
  var thisUrl = ''; /*Хранит текущий URL*/
  var rectSelect = null; /*Хранит индекс перетаскиваемого элемента*/
  var counter = 0; /*Считает количество фигур, помещенных на холст*/
  var xyTemp = null; /*Содержит начальные координаты выбранной фигуры*/  
  var rects = []; /*Массив фигур*/
  var lockedPl = []; /*Массив занятых секторов*/
  var rectsImagine = []; /*Массив представления о заполнении*/

  canvas.onmousedown = mouseDown;
  canvas.onmouseup = mouseUp;
  canvas.onmousemove = mouseMove;
  canvas.onwheel = mouseWheel;

  /*Размеры области размещения фигур*/
  var placeWidth;
  var placeHeight;

  function init(reqA, reqF){
    if(thisUrl.length == 0)
    thisUrl = thisUrl+"#/"+reqA[(reqA.length)-1]+"/";
    sendRequest(reqA);
    sendRequest(reqF);
  }
  function sendRequest(url){
    axios.get(url)
      .then(function (response) {
        (url[5] == 'a') ? setArea(response.data) : createRects(response.data);
      })
      .catch(function (error) {
          console.log("ERROR:"+error.message);
      });
  }

  /*Принимает параметры Canvas*/
  function setArea(input){
    area.width = +input.width;
    area.height = +input.height;
    area.step = +input.step;
    area.marginRight = +input.marginRight;
    canvas.style.backgroundColor = input.bgColor;
    canvas.width = area.width*area.step+area.marginRight;
    canvas.height = area.height*area.step;
    placeWidth = canvas.width-area.marginRight;
    placeHeight = area.height*area.step;
  }
  function addRect(){
    axios.get('/src/mongo.php?set='+(rects.length+1))
    .then(function (response) {
      var str = JSON.stringify(response.data);
      var input = JSON.parse(str);
      rects.push({id: rects.length, dbId: input[0][0].id, x: canvas.width-350,y: 100,w: input[0][0].width*area.step,
        h: input[0][0].height*area.step, color: input[0][0].color });
        getImaginFull();
    })
    .catch(function (error) {
        console.log("ERROR:"+error.message);
    });
  }

  function setRectPos(){
    for(var i = 0; i < rects.length; i++){
      if(rects[i].x > placeWidth){
        rects[i].x = placeWidth+area.step*0.5;
        switch(rects[i].w){
          case (1*area.step):
          if(rects[i].h == 1*area.step)
            rects[i].y = area.step+offsetRectsY;
          else {
              rects[i].y = area.step*1+offsetRectsY;
              rects[i].x = placeWidth+area.step*2;
            }
          break;
          case (2*area.step):
          if(rects[i].h == 1*area.step)
            rects[i].y = area.step*4+offsetRectsY;
          else
            rects[i].y = area.step*6+offsetRectsY;
          break;
          case (3*area.step):
          if(rects[i].h == 2*area.step)
            rects[i].y = area.step*9+offsetRectsY;
          else
            rects[i].y = area.step*12+offsetRectsY;
          break;
        }
      }
    }
    draw();
  }

  function createRects(resData){
    var str = JSON.stringify(resData);
    var input = JSON.parse(str);
    rects.length = 0;
    rectsImagine.length = 0;
    lockedPl.length = 0;
    for(var i = 0, id = 0; i < input.length; i++, id++){
      rects.push({id: id, dbId: input[i][0].id, x: canvas.width-350,y: 100,w: input[i][0].width*area.step,
        h: input[i][0].height*area.step, color: input[i][0].color });
    }
    getImagin();
  }

  function sortRects(rects){
    for(var i = 0, length = rects.length; i<length; i++){
      if(rects[i].w < rects[i].h){
        swapRectWH(rects, i);
      }
    }	
  function compareWidth(rectA, rectB) {
    if(rectA.w == rectB.w) return rectB.h - rectA.h;
      return rectB.w - rectA.w;
    }
    rects.sort(compareWidth);
  }
  function swapRectWH(array, index){
    var temp = 0;
    temp = array[index].w;
    array[index].w = array[index].h;
    array[index].h = temp;
  }
  /*Функция заполняет массив lockedPl координатами занятых ячеек и отправляет
  true, если ячейки заняты - false*/
  function takeArea(x, y, index, readOnly){
    if(x < (placeWidth)){
      var countInputs = 0;
      for(var i = x; i < x+rects[index].w; i+= area.step){
        for(var j = y; j < y+rects[index].h; j+= area.step){
          for(var k = 0; k<lockedPl.length; k++){
              if((counter>0) && (lockedPl[k].x === i) && (lockedPl[k].y === j)){
                if(!readOnly){
                lockedPl.splice(((lockedPl.length)-countInputs), countInputs);
              }
              return false;
            } else if((x+rects[index].w > placeWidth) || (y+rects[index].h>placeHeight)) {
              return false;
            }
          }
          if(!readOnly){ lockedPl.push({ x: i, y: j }); }
          countInputs++;
        }
      }
    }
    return true;
  }
  /*Функция проверяет, заполнена ли область фигурами. Сравнивает площадь области
  с площадью, занимаемой фигурами, находящимися в пределах области.*/
  function isFilled(){
    var areaOfShapes = 0;
    for(var i=0, length = rects.length; i < length; i++){
      if(rects[i].x < placeWidth){
        areaOfShapes += rects[i].w * rects[i].h;
      }
    }
    if(areaOfShapes >= area.step*area.width*area.height*area.step){
      return true;
    }
    return false;
  }
  function drawGrid(){
    var pen = {
      x: 0,
      y: canvas.height
    };
    for(var i=0; i <= area.width; i++){
      ctx.moveTo(pen.x, 0);
      ctx.lineTo(pen.x, pen.y);
      pen.x+=(area.step);
    }
    pen.x=placeWidth, pen.y = 0;
    for(var i=0; i <= area.height; i++){
      ctx.moveTo(0, pen.y);
      ctx.lineTo(pen.x, pen.y);
      pen.y+=(area.step);
    }
    ctx.strokeStyle = "#888";
    ctx.stroke();
  }
  /*Функция заполняет массив прямоугольников. В качестве аргументов
  принимает строку, состоящую из числовой последовательности, характеризующей
  размер фигур(измеряется в секторах) createRects('width, height... etc')*/
  function draw(){
    ctx.clearRect(0,0,canvas.width,canvas.height);
    drawGrid();
    for(var i=0, length = rects.length; i<length; i++){
      rects[i].x>(placeWidth)?ctx.fillStyle=
          "rgba("+rects[i].color+", 0.5)":ctx.fillStyle="rgba("+rects[i].color+", 1)";
      ctx.fillRect(rects[i].x, rects[i].y, rects[i].w, rects[i].h);
    }
  }
  function mouseDown(e){
    for(var i=0, length = rects.length; i<length; i++){
      if(((e.offsetX>rects[i].x) && (e.offsetX<rects[i].x+rects[i].w)) &&
       ((e.offsetY>rects[i].y) && (e.offsetY<rects[i].y+rects[i].h))){
        xyTemp = { x: rects[i].x, y: rects[i].y };
        rectSelect = i;
      }
    }
  }
  function mouseMove(e){
    if(rectSelect!=null){
      rects[rectSelect].x = e.offsetX;
      rects[rectSelect].y = e.offsetY;
    }
    drawGrid();
    draw();
  }
  /*Функция формирует представление о правильном положении фигур в области*/
  function getImagin(){
    sortRects(rects);
    for(var i = 0; i < rects.length; i++){
      stop = false;
      for(var y = 0; y < placeHeight && !stop; y+=area.step){
        for(var x = 0; x < placeWidth && !stop; x+=area.step){
          if(takeArea(x, y, i, false)){
            rectsImagine.push({id: rects[i].id, dbId: rects[i].dbId, x: x, y: y, w: rects[i].w, h: rects[i].h});
            counter++;
            stop = true;
          }
        }
      }
      if(!stop){
        swapRectWH(rects, i);
        for(var y = 0; y < placeHeight && !stop; y+=area.step){
          for(var x = 0; x < placeWidth && !stop; x+=area.step){
            if(takeArea(x, y, i, false)){
              rectsImagine.push({id: rects[i].id, dbId: rects[i].dbId, x: x, y: y, w: rects[i].w, h: rects[i].h});
              counter++;
              stop = true;
            }
          }
        }
      }
    }
    getImaginFull();
    lockedPl = [-1];
  }
  function getImaginFull(){
    var isContain = false;
    for(var i = 0; i < rects.length; i++){
      for(var j = 0; j < rectsImagine.length; j++){
        if(rects[i].id == rectsImagine[j].id){
          isContain = true;
        }
      }
        if(isContain == false){
          for(var j = 0; j < rectsImagine.length; j++){
            if((rects[i].w == rectsImagine[j].w) && (rects[i].h == rectsImagine[j].h)){
              rectsImagine.push({id: rects[i].id, dbId: rects[i].dbId, x: rectsImagine[j].x, y: rectsImagine[j].y});
            }
          }
        }
        isContain = false;
    }
    setRectPos();
  }

  function isLocked(x, y){
    if(lockedPl.length != 0){
      for(var i = 0; i < lockedPl.length; i++){
        if((lockedPl[i].x == x) && (lockedPl[i].y == y)){
          return i;
        }
      }
    }
    return false;
  }

 function mouseUp(e){
    if(rectSelect!=null && (rects[rectSelect].x < placeWidth)){
      for(var i = 0; i<rectsImagine.length; i++){
        if(rects[rectSelect].id === rectsImagine[i].id){
          if(isLocked(rectsImagine[i].x,rectsImagine[i].y) == false){
            rects[rectSelect].x = rectsImagine[i].x;
            rects[rectSelect].y = rectsImagine[i].y;
            lockedPl.push({x: rects[rectSelect].x, y: rects[rectSelect].y});
            break;
          }
        } else {
          rects[rectSelect].x = canvas.width - 350;
          rects[rectSelect].y = 100;
        }
      }
    }
    if(rectSelect != null && rects[rectSelect].x > placeWidth){
      if(isLocked(xyTemp.x, xyTemp.y) != false){
        lockedPl.splice(isLocked(xyTemp.x, xyTemp.y), 1);
      }
    }
    thisUrl = thisUrl.substr(0, 4);
    for(var i = 0; i < rects.length; i++){
      if(rects[i].x < placeWidth){
        thisUrl+=rects[i].dbId+',';
      }
    }
    setRectPos();
    history.pushState(null, null, thisUrl);
    isFilled()?console.log("Область заполнена"):console.log("Область ещё не заполнена");
    rectSelect = null;
  }
  function mouseWheel(e){
      if(rectSelect==null){
        offsetRectsY+=(-(e.deltaY/2));
        setRectPos();
      }
  }

  function restoreRects(input){
    id = String(input);
    var splited = id.split(',');
    if(id!="_"){
      for(var i = 0; i < splited.length-1; i++){
        for(var j = 0; j < rectsImagine.length; j++){
          if(splited[i] == rectsImagine[j].dbId){
            for(var k = 0; k < rects.length; k++){
              if(rectsImagine[j].dbId == rects[k].dbId){
                rects[k].x = rectsImagine[j].x;
                rects[k].y = rectsImagine[j].y;
                thisUrl+=rects[k].dbId+',';
                history.pushState(null, null, thisUrl);
              }
            }
          }
        }
      }
    }
    else {
      draw();
      return;
    }
    draw();
  }

  return{
    init: init,
    sendRequest: sendRequest,
    restoreRects: restoreRects,
    getImagin: getImagin,
    addRect: addRect
  };
};
