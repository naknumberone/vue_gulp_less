var gulp = require("gulp");
var less = require('gulp-less');
var path = require('path');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var order = require("gulp-order");

gulp.task('less', function () {
  return gulp.src('./less/styles.less')
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
    .pipe(gulp.dest('./assets'));
});

gulp.task('js', function () {
  gulp
    .src(['./js/areaFilling.js', './js/app.js'])
    .pipe(order([
    "areaFilling.js",
    "app.js"]))
    .pipe(concat('app-prod.js'))
    //.pipe(uglify())
    .pipe(gulp.dest('./assets'))
});

gulp.task("default", ["less", "js"]);
